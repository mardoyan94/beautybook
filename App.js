import React from 'react';
import MyApp from './app/app'
import Reducer from './app/reducers/rootReducer'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

const store = createStore(Reducer)

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MyApp/>
      </Provider>
    );
  }
}
