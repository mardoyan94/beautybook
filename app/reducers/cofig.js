config={
    logOut: true
}

export default function createConfig(state=config, action){
    if(action.type=='SET_CONFIG'){
        return {...state, logOut: action.logOut}
    }
    return state;
}