import React from 'react';
import { 
  StyleSheet,
   View,
  Image
  } from 'react-native';



export default class Loading extends React.Component {

  render() {
       return (<View style={styles.container}>
          <Image
          style={{
              flex: 1,
              width: '100%'
          }}
          source={require('./assets/splash.png')}
          />
        </View>
      )

}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
