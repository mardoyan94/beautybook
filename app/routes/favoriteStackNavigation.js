import { createStackNavigator } from 'react-navigation';
import {
    FavoriteList
} from '../views/favorite'

const FavoriteStachNavigation = createStackNavigator({
    favorite: {
      screen: FavoriteList
    },
  },
{
  headerMode: 'none'
});

export default FavoriteStachNavigation;