//import Ionicons from 'react-native-vector-icons/Ionicons';
import React from 'react';
import { Icon } from 'react-native-elements'
import { createBottomTabNavigator } from 'react-navigation';
import HomeStachNavigation from './homeStackNavigation'
import FavoriteStachNavigation from './favoriteStackNavigation'
import {NotificationsList} from '../views/notifications'
import { Profile } from '../views/profile'

const MainTabNavigation =  createBottomTabNavigator(
  {
    Home: HomeStachNavigation,
    Favorite: FavoriteStachNavigation,
    notifications: NotificationsList,
    profile: Profile
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Settings') {
          iconName = `ios-options${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Icon name='home' size={25} color={tintColor} />;
      },
      tabBarLabel:()=>null
    }),
    tabBarOptions: {
      
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
      swipeEnabled: false,
      style:{
        backgroundColor: '#fff',
        borderTopWidth:0,
      }
    },
  }
);

export default MainTabNavigation;