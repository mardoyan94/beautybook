import { createStackNavigator } from 'react-navigation';
import {
  Home,
  CategorySalons,
  SearchSettings,
  Photo
} from '../views/home'

const HomeStachNavigation = createStackNavigator({
    home: {
      screen: Home,
    },
    categorySalons:{
      screen: CategorySalons,
    },
    searchSettings:{
      screen: SearchSettings
    },
    photo: {
      screen: Photo
    }
  },
{
  headerMode: 'none',
});

HomeStachNavigation.navigationOptions = ({ navigation }) => {
  const routes = navigation.state.routes[2]
  let tabBarVisible = true;
  if(routes){
    if(routes.params.tabBarVisible){
      tabBarVisible = false;
    }
    
  }

  return {
    tabBarVisible,
  };
};

export default HomeStachNavigation;