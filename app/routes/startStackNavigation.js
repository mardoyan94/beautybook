import { createStackNavigator } from 'react-navigation';
import {
     SelectGender,
     EnableGprs,
     EnterNumber,
     Verefication,
     Password,
     Login,
     PasswordRecovery,
     NewPassword
     } from '../views/startWork'

const StartStackNavigation = createStackNavigator({
    gender: {
      screen: SelectGender
    },
    gprs:{
      screen: EnableGprs
    },
    enterNumber:{
      screen: EnterNumber
    },
    verefi:{
      screen: Verefication
    },
    password:{
      screen: Password
    },
    login: {
      screen: Login
    },
    passwordRecovery:{
      screen: PasswordRecovery
    },
    newPassword:{
      screen: NewPassword
    }
  },
  {
    initialRouteName: 'gender',
    headerMode: 'none'
}
);
export default StartStackNavigation;