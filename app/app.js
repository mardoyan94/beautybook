import React from 'react';
import { 
  StyleSheet,
   View,
  AsyncStorage
  } from 'react-native';
import StartStackNavigation from './routes/startStackNavigation'
import MainTabNavigation from './routes/mainTabNavigation'
import { YellowBox } from 'react-native'
import { AppLoading} from 'expo';
import Loading from './loading'

import { connect } from 'react-redux';

class MyApp extends React.Component {
  constructor(props){
    super(props)
    this.state={
      loaded:false
    }
  }
  componentWillMount(){
    console.log('componentWillMount');
    
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated'])
    this.getTokens().then((value)=>{
      this.setState({
        loaded: true
      })
      this.props.dispatch({type:'SET_CONFIG', logOut: value})
    }).catch((error)=>{
      console.log(error);
      
    })
  }

async getTokens(){
  try{
    let tokens = await AsyncStorage.getItem('token')
    if(tokens!=null){
      return false
    }
    return true
  }
  catch(error){
    console.log(error);
    
  }
}

  render() {
    if(!this.state.loaded){
      return (<Loading/>)
    }
    if(this.props.config.logOut){
      return (
        <View style={styles.container}>
          <StartStackNavigation/>
        </View>
      );
    }
    else{
      return(
        <View style={styles.container}>
          <MainTabNavigation/>
        </View>
      );
    }
    
  }
}

export default connect(
  ({config})=>({config})
)(MyApp)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
