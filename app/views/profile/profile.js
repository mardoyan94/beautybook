import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    AsyncStorage
} from 'react-native';
import { Icon } from 'react-native-elements'
import { LinearGradient } from 'expo';
import { connect } from 'react-redux';

class ProfileClass extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    async clearStorage() {
        try {
            await AsyncStorage.clear()
        }
        catch (error) {
            console.log(error);

        }
    }

    logOut(){
        this.clearStorage().then(()=>{
            this.props.dispatch({type:'SET_CONFIG', logOut: true})
        })
    }

    render() {
        return (

            <View style={styles.container}>

                <View style={styles.header}>
                    <Text style={styles.headerTitle}>
                        Профиль
        </Text>
                </View>
                <ScrollView style={{ width: '100%' }}>
                    <View style={styles.scrollContent}>
                        <View
                            style={styles.nameCardContainer}
                        >
                            <LinearGradient
                                style={styles.gradientContainer}
                                start={[1, 1]}
                                //end={{ x: 0.5, y: 0.5 }}
                                locations={[0.3, 0.7]}
                                colors={['#7c52fd', '#1998ff']}
                            >
                                <View>
                                    <Text style={styles.nameText}>Константин</Text>
                                    <Text style={styles.nameText}>Константинопольский</Text>
                                </View>
                                <TouchableOpacity>
                                    <Icon
                                        name='settings'
                                        size={30}
                                        color='#fff'
                                    />
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                        <View style={styles.userInfoContainer}>
                            <Text style={styles.userInfoText}>
                                Пол: мужской
                            </Text>
                            <Text style={styles.userInfoText}>
                                E-mail: unic101@gmail.com
                            </Text>
                            <Text style={styles.userInfoText}>
                                Телефон: +7(999) 999-99-99
                            </Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => { }}
                            style={styles.shareBttContainer}
                        >
                            <LinearGradient
                                style={styles.shareBttGradient}
                                start={[1, 1]}
                                //end={{ x: 0.5, y: 0.5 }}
                                locations={[0.3, 0.7]}
                                colors={['#7c52fd', '#1998ff']}
                            >

                                <Text style={styles.bttText}>Поделиться приложением</Text>
                                <Icon
                                    containerStyle={{ marginLeft: 5 }}
                                    name='share'
                                    color='#fff'
                                    size={24}
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                        <View style={{ width: '93%' }}>
                            <Text style={styles.reviewTitle}>Оценить приложение</Text>
                            <View style={styles.reviewCard}>
                                <Text
                                    style={styles.reviewCardDes}>
                                    Если вам понравилось наше приложение, оставьте свою оценку и напишите отзыв. Это поможет нам сделать приложение ешо лучше!
                        </Text>
                                <TouchableOpacity
                                    style={styles.reviewBttContainer}
                                >
                                    <LinearGradient
                                        style={styles.reviewBtt}
                                        start={[1, 1]}
                                        //end={{ x: 0.5, y: 0.5 }}
                                        locations={[0.3, 0.7]}
                                        colors={['#7c52fd', '#1998ff']}
                                    >
                                        <Text style={styles.reviewBttText}>Оставить отзыв</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={styles.itemBtt}
                        >
                            <Text style={styles.itemBttText}>
                                Помощь/обратная связь
                        </Text>
                            <Icon
                                containerStyle={{
                                    marginRight: '5%',
                                    opacity: 0.7
                                }}
                                name='keyboard-arrow-right'
                                size={35}
                                color='silver'
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.itemBtt}
                        >
                            <Text style={styles.itemBttText}>
                                Конфиденциальность и условия
                        </Text>
                            <Icon
                                containerStyle={{
                                    marginRight: '5%',
                                    opacity: 0.7
                                }}
                                name='keyboard-arrow-right'
                                size={35}
                                color='silver'
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.logOut()}
                            style={styles.itemBtt}
                        >
                            <Text style={styles.itemBttTextQuit}>
                                Выход из аккаунта
                        </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export const Profile = connect()(ProfileClass)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    header: {
        marginTop: 26,
        marginBottom: 12,
        width: '100%',
        height: 50,
        justifyContent: 'center',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: '5%',
    },
    scrollContent: {
        width: '100%',
        alignItems: 'center',
    },
    nameCardContainer: {
        width: '90%',
        height: 94
    },
    gradientContainer: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },
    nameText: {
        color: '#fff',
        fontSize: 19,
        fontWeight: 'bold'
    },
    userInfoContainer: {
        marginTop: 23,
        width: '100%'
    },
    userInfoText: {
        marginBottom: 15,
        marginLeft: '10%',
        color: 'black',
        fontSize: 16
    },
    shareBttContainer: {
        marginTop: 7,
        width: '90%',
        height: 50
    },
    shareBttGradient: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText: {
        color: '#fff',
        fontSize: 16
    },
    reviewTitle: {
        marginTop: 20,
        marginBottom: 8,
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: '3%',
    },
    reviewCard: {
        marginBottom: 17,
        width: '100%',
        minHeight: 172,
        backgroundColor: '#cfe7ff',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    reviewCardDes: {
        marginHorizontal: 23,
    },
    reviewBttContainer: {
        marginTop: 24,
        height: 50,
        width: '65%',
    },

    reviewBtt: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    reviewBttText: {
        color: '#fff',
        fontSize: 18
    },
    itemBtt: {
        marginBottom: 3,
        width: '100%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    itemBttText: {
        fontSize: 16,
        marginLeft: '4%',
    },
    itemBttTextQuit: {
        fontSize: 16,
        marginLeft: '4%',
        color: 'red'
    }
});
