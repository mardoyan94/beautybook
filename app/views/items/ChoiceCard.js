import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { Icon } from 'react-native-elements'
import { AirbnbRating } from 'react-native-ratings';

export default class ChoceCard extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (<TouchableOpacity
         style={[
             styles.choiceCardContainer,
             this.props.priceRating ? { marginBottom: 0 } : null
            ]}
         >
            <Image
                style={styles.image}
                source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
            />
            <View style={styles.choiceCardInfoContainer}>

                <Text style={styles.choceCardInfoTitle}>
                    Kregina studio
        </Text>
                <Text style={styles.popularCardInfoDistance}>
                    3 км от вас
        </Text>
                <View style={styles.ratingRow}>
                    <AirbnbRating
                        showRating={false}
                        count={5}
                        defaultRating={5}
                        size={10}
                    />
                    <Text style={styles.ratingText}>4,8/</Text>
                    <Icon
                        name='person'
                        size={14}
                        color='#fff'
                    />
                    <Text style={styles.ratingText}>98</Text>
                </View>
            </View>
            <TouchableOpacity
                style={styles.choiceIconContainer}
            >
                {this.props.favorite ?
                    <Icon
                        name='favorite'
                        size={25}
                        color='#fff'
                    /> :
                    <Icon
                        name='favorite-border'
                        size={25}
                        color='#fff'
                    />}
            </TouchableOpacity>
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
    choiceCardContainer: {
        marginBottom: 12,
        width: '90%',
        backgroundColor: 'silver',
        height: 79,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 8,
    },
    choiceCardInfoContainer: {
        justifyContent: 'center',
        position: 'absolute',
        top: 9,
        left: 30,
    },
    choceCardInfoTitle: {
        fontSize: 18,
        color: '#fff',
        fontWeight: 'bold'
    },
    popularCardInfoDistance: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 12,

    },
    ratingRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ratingText: {
        color: '#fff',
        fontSize: 13
    },
    choiceIconContainer: {
        position: 'absolute',
        right: 0,
        top: 13,
        padding: 8,
    },
});
