import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    ScrollView,
    Keyboard,
    TextInput
} from 'react-native';
import { Icon } from 'react-native-elements'

export class Photo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            keyboardHeight: 0,
          keyboardOpen: false,
        }
    }


    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          this.keyboardDidShowListener,
        );
        this.keyboardHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this.keyboardHideListener,
          );
      }
      keyboardDidShowListener = e => {
        this.setState({
          keyboardOpen: true,
          keyboardHeight: e.endCoordinates.height,
        });
      };

      keyboardHideListener = () => {
        this.setState({
            keyboardHeight: 0,
          keyboardOpen: false,
        });
      };

      _renderComment(){
          let arr = ['класс!', 'выглядеть шикарно!']
          return arr.map((item, i)=>(
            <View key={i} style={styles.commentContainer}>
            <Text style={styles.name}>alexa99 <Text style={styles.comment}>{item}</Text></Text>
            </View>
          ))
      }

    render() {
        return (
        
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <Icon
                        onPress={() => this.props.navigation.goBack()}
                        containerStyle={{
                            opacity: 0.7,
                            marginLeft: '2%'
                        }}
                        name='keyboard-arrow-left'
                        size={35}
                        color='silver'
                    />
                    <Text style={styles.headerTitle}>
                        Фото
                    </Text>
                </View>
            </View>
            <ScrollView
            keyboardShouldPersistTaps='handled'
            style={{
                 marginBottom: this.state.keyboardHeight,
                  backgroundColor: '#fff',
                  width: '100%'
                 }} 
            >
            <View style={styles.photoContainer}>
            <Image
            style={styles.image}
            source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
            />
            </View>
            <View style={styles.likeRow}>
            <Icon 
            name='thumb-up'
            color='#2391ff'
            size={20}
            />
            <Text style={{fontSize: 18}}>22</Text>
            </View>
            <View style={styles.descContainer}>
            <Text style={styles.name}>annabell <Text style={styles.comment}>фото клиента с работой нашего мастера, Галины Селезнёвой</Text></Text>
            </View>
            {this._renderComment()}
            <View style={styles.inputContainer}>
            <TextInput
            style={styles.input}
            placeholder='ваш комментарий'
            />
            </View>
            </ScrollView>
        </View>
        
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        marginTop: 26,
        marginBottom: 5,
        width: '100%',
        height: 50,
        justifyContent: 'center',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: -5,
    },
    photoContainer:{
        backgroundColor: 'silver',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 355
    },
    image:{
        flex: 1,
        width: '100%'
    },
    likeRow:{
        marginTop: 12,
        marginLeft: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    descContainer:{
        marginLeft: 16,
        marginTop: 5,
        marginBottom: 10,
        marginRight: 50,
    },
    name:{
        fontWeight: 'bold'
    },
    comment:{
        marginVertical: 5,
        fontWeight: '100',
        color: 'rgba(0,0,0,0.8)'
    },
    commentContainer:{
        marginLeft: 16,
        marginTop: 5,
        marginRight: 50,
    },
    inputContainer:{
        marginTop: 15,
        width: '90%',
        height: 100,
        borderColor: 'silver',
        borderWidth: 1,
        borderRadius: 8,
        alignSelf: 'center',
    },
    input:{
        flex: 1,
    }
});
