import React from 'react';
import { 
  StyleSheet,
   View,
  Image,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback
  } from 'react-native';
import { Icon } from 'react-native-elements'
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { LinearGradient } from 'expo';

export class SearchSettings extends React.Component {
  constructor(props){
    super(props)
    this.state={
      sliderValue: 3,
      dayButtonIndex:null,
      priceButtonIndex: null
    }
  }

  _renderDays(){
    let arr = [ 'Сейчас', 'Сегодня', 'Завтра' ]
    return arr.map((item, i)=>(
      <View key={i} >{this.state.dayButtonIndex==i ? 
        <TouchableOpacity
        style={{
          height: 26,
          marginVertical: 3,
        }}
        >
      <LinearGradient
                    style={styles.dayButtonActiv}
                    start={[1, 1]}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                >
     <Text style={styles.dayButtonTextActiv}>{item}</Text>
     <TouchableOpacity
     onPress={()=>this.setState({
      dayButtonIndex: null
    })}
     >
        <Icon
          containerStyle={{
             marginRight: 8,
             marginLeft: 7
          }}
             name='close'
            size={17}
            color='#fff'
        />
      </TouchableOpacity>
      </LinearGradient>
     </TouchableOpacity> :
      <TouchableOpacity
      onPress={()=>this.setState({
        dayButtonIndex: i
      })}
      style={styles.dayButton}>
     <Text style={styles.dayButtonText}>{item}</Text>
     </TouchableOpacity>
    }
      </View>
    ))
  }

  _renderPrice(){
    let arr = [ '300-500₽', '500-1000₽', '1000-2000₽', '>2000₽' ]
    return arr.map((item, i)=>(
      <View key={i} >{this.state.priceButtonIndex==i ? 
        <TouchableOpacity
        style={{
          height: 26,
          marginVertical: 3,
        }}
        >
      <LinearGradient
                    style={styles.dayButtonActiv}
                    start={[1, 1]}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                >
     <Text style={styles.dayButtonTextActiv}>{item}</Text>
     <TouchableOpacity
     onPress={()=>this.setState({
      priceButtonIndex: null
    })}
     >
        <Icon
          containerStyle={{
             marginRight: 8,
             marginLeft: 7
          }}
             name='close'
            size={17}
            color='#fff'
        />
      </TouchableOpacity>
      </LinearGradient>
     </TouchableOpacity> :
      <TouchableOpacity
      onPress={()=>this.setState({
        priceButtonIndex: i
      })}
      style={styles.dayButton}>
     <Text style={styles.dayButtonText}>{item}</Text>
     </TouchableOpacity>
    }
      </View>
    ))
  }

  render() {
    const params = this.props.navigation.state.params
       return (<View style={styles.container}>
                  <View style={styles.header}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <Text style={styles.headerTitle}>
                    Косметология
                       
                    </Text>
                    <TouchableOpacity
                    style={styles.headerBttContainer}
                    >
                    <Text style={styles.headerText}> {params.name.toLowerCase()}</Text>
                    <Icon
                    containerStyle={{
                      marginTop: 2,
                      marginHorizontal: 3
                    }}
                        name='arrow-drop-down'
                        color='#2391ff'
                        size={20}
                        />
                        </TouchableOpacity>
                </View>
            </View>
            <Text style={styles.sliderTitle}>В радиусе {Math.floor(this.state.sliderValue)} км ой меня</Text>
            <MultiSlider
            min={0}
            max={10}
            onValuesChange={(value)=>this.setState({
              sliderValue: value[0]
            })}
          selectedStyle={{
            backgroundColor: '#d9d9d9',
          }}
          unselectedStyle={{
            backgroundColor: '#d9d9d9',
          }}
          values={[this.state.sliderValue]}
          containerStyle={{
            height:42,
            alignSelf: 'center',
          }}
          trackStyle={{
            height:4,
       
          }}
          customMarker={()=>(<Image
            style={{
              marginTop: 4,
              height: 23,
              width: 23
            }}
            source={require('../../assets/Blue_dot.png')}
            />)}
          sliderLength={310}
        />
        <TouchableOpacity style={styles.activTextContainer}>
        <Text style={styles.activText}>Выбрать другое место на карте</Text>
        <View style={styles.activTextLine}></View>
        </TouchableOpacity>
        <View style={styles.dayButtonsContainer}>
        {this._renderDays()}
        </View>
         <View style={styles.textRow}>
        <Text style={styles.orText}>или </Text>
        <TouchableOpacity style={styles.activTextContainer2}>
        <Text style={styles.activText}>выберите дату и время</Text>
        <View style={styles.activTextLine}></View>
        </TouchableOpacity>
        </View> 
        <View style={styles.dayButtonsContainer}>
        {this._renderPrice()}
        </View>
        <TouchableNativeFeedback>
          <View style={styles.itemBtt}>
          <Text style={styles.itemBttText}>Выбор бренда косметики</Text>
          </View>
        </TouchableNativeFeedback>
        <View style={styles.bttContainerOuter}>
        <TouchableOpacity
        onPress={() => { }}
              style={styles.bttContainer}
        >
              <LinearGradient
             style={styles.btt}
             start={[1, 1]}
             //end={{ x: 0.5, y: 0.5 }}
             locations={[0.3, 0.7]}
             colors={['#7c52fd', '#1998ff']}
           >
             <Text style={styles.bttText}>Найти</Text>
           </LinearGradient>
         </TouchableOpacity>
         </View>
        </View>
     
      )

}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    backgroundColor: '#fff'
  },
  header: {
    marginTop: 26,
    marginBottom: 19,
    width: '100%',
    height: 50,
    justifyContent: 'center',
},
headerTitle: {
  fontSize: 20,
  fontWeight: 'bold',
  color: 'black',
  marginLeft: '9%',
},
headerText: {
    fontSize: 15,
    fontWeight: '100',
    color: 'rgba(0,0,0,0.6)',
},
headerBttContainer: {
  height: 30,
  marginTop: 4,
  flexDirection: 'row',
  alignItems: 'center',
},
sliderTitle:{
  marginLeft: '6%',
  marginTop: '6%',
  fontSize: 16
},
sliderGradient:{
  width: '100%',
  height: '100%',
},
activTextContainer:{
  marginLeft: '6%',
  alignSelf: 'flex-start'
},
activText:{
  fontSize: 16,
  color: '#2391ff'
},
activTextLine:{
  marginTop: -4,
  height: 1,
  borderBottomColor: '#2391ff',
  borderBottomWidth: 1,
},
dayButtonsContainer:{
  marginBottom: '3%',
  marginTop: '13%',
  marginHorizontal: '5%',
  width: '88%',
  flexDirection: 'row',
  flexWrap: 'wrap',
},
dayButton:{
  marginVertical: 3,
  alignItems: 'center',
  marginHorizontal: 3,
  height: 26,
  borderRadius: 13,
  borderColor: '#2391ff',
  borderWidth: 1.3,
},
dayButtonText:{
  marginHorizontal: 13,
  fontSize: 15.5,
  color: '#2391ff'
},
dayButtonActiv:{
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  marginHorizontal: 3,
  height: 26,
  borderRadius: 13,
},
dayButtonTextActiv:{
  marginBottom: 3,
  marginLeft: 15,
  fontSize: 15.5,
  color: '#fff'
},
orText:{
  fontSize: 16,
},
textRow:{
 marginLeft: '6%',
 alignSelf: 'flex-start',
  flexDirection: 'row',
  justifyContent: 'center',
},
activTextContainer2:{
  alignSelf: 'flex-start'
},
itemBtt:{
  width: '100%',
  height: 50,
  alignItems: 'center',
  justifyContent: 'space-between',
  flexDirection: 'row',
},
itemBttText:{
  marginLeft: '6%',
  fontSize: 16
},
bttContainer: {
  height: 50,
  width: '62%',
},

btt: {
  width: '100%',
  height: '100%',
  borderRadius: 10,
  justifyContent: 'center',
  alignItems: 'center',
},
bttText: {
  color: '#fff',
  fontSize: 18
},
bttContainerOuter:{
  width: '100%',
  alignItems: 'center',
  position: 'absolute',
  bottom: '3%'
}

});
