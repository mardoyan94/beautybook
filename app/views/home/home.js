import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo';
import { AirbnbRating } from 'react-native-ratings';
import { Icon } from 'react-native-elements'
import ChoceCard from '../items/ChoiceCard'

class HomeClass extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categoryIndex: null
        }
    }

    _renderPopularCard() {
        let arr = [1, 2, 3, 4, 5]
        return arr.map((item, i) => (
            <TouchableOpacity key={i} style={styles.popularCardContainer}>
                <Image
                    style={styles.image}
                    source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                />
                <View style={styles.popularCardInfoContainer}>
                    <Text style={styles.popularCardInfoTitle}>
                        Kregina studio
            </Text>
                    <Text style={styles.popularCardInfoDistance}>
                        3 км от вас
            </Text>
                    <View style={styles.ratingRow}>
                        <AirbnbRating
                            showRating={false}
                            count={5}
                            defaultRating={5}
                            size={10}
                        />
                        <Text style={styles.ratingText}>4,8/</Text>
                        <Icon
                            name='person'
                            size={14}
                            color='#fff'
                        />
                        <Text style={styles.ratingText}>98</Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={styles.favoriteIconContainer}
                >
                    <Icon
                        name='favorite-border'
                        size={25}
                        color='#fff'
                    />
                </TouchableOpacity>
            </TouchableOpacity>
        ))
    }

    _renderNewsCard() {
        let arr = [1, 2, 3, 4, 5]
        return arr.map((item, i) => (
            <TouchableOpacity key={i} style={styles.popularCardContainer}>
                <Image
                    style={styles.image}
                    source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                />
                <View style={styles.popularCardInfoContainer}>
                    <Text style={styles.popularCardInfoTitle}>
                        Сравнение брендов красок
            </Text>
                    <View style={styles.newCardRatingRow}>
                        <Text style={styles.newsCardRatingText}>BeautyStore</Text>
                        <View style={styles.newsCardRatingIconContainer}>
                            <Icon
                                name='thumb-up'
                                color='#fff'
                                size={13}
                            />
                            <Text style={styles.newsCardRatingTextCount}>22</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        ))
    }

    _renderChoiceCard(arr) {
        return arr.map((item, i) => (
            <ChoceCard key={i} item={item} />
        ))
    }

    _renderNewPhotosCard() {
        let arr = [1, 2, 3, 4, 5]
        return arr.map((item, i) => (
            <TouchableOpacity
            onPress={()=>this.props.navigation.navigate('photo')}
             key={i}
              style={styles.popularCardContainer}>
                <Image
                    style={styles.image}
                    source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                />
                <View style={styles.popularCardInfoContainer}>
                    <View style={styles.newCardRatingRow}>
                        <Text style={styles.newsCardRatingText}>BeautyStore</Text>
                        <View style={styles.newsCardRatingIconContainer}>
                            <Icon
                                name='thumb-up'
                                color='#fff'
                                size={13}
                            />
                            <Text style={styles.newsCardRatingTextCount}>22</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        ))
    }

    _renderCategoryButtons() {
        let arr = ['Стрижка и окраска', 'Массаж', 'SPA уход', 'Брови и ресницы', 'Косметология', 'Makeup']

        if (this.state.categoryIndex == null) {
            return arr.map((item, i) => (
                <TouchableOpacity
                    key={i}
                    onPress={() => this.setState({
                        categoryIndex: i
                    })}
                >
                    <LinearGradient
                        style={styles.btt}
                        start={[1, 1]}
                        locations={[0.3, 0.7]}
                        colors={['#7c52fd', '#1998ff']}
                    >
                        <Text style={styles.bttText}>{item}</Text>
                    </LinearGradient>
                </TouchableOpacity>
            ))
        }
        else {
            return (<TouchableOpacity>
                <LinearGradient
                    style={styles.btt}
                    start={[1, 1]}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                >
                    <Text style={styles.bttText}>{arr[this.state.categoryIndex]}</Text>
                    <TouchableOpacity
                        onPress={() => this.setState({
                            categoryIndex: null
                        })}
                    >
                        <Icon
                            containerStyle={{ marginRight: 10 }}
                            name='close'
                            size={17}
                            color='#fff'
                        />
                    </TouchableOpacity>
                </LinearGradient>
            </TouchableOpacity>)
        }
    }

    _renderSubcategorys() {
        let arr = ['Эстетическая', 'Контурная пластика', 'Аппаратная', 'Эпиляция', 'Инъекционная', 'Татуаж']
        return this.state.categoryIndex != null ?
            arr.map((item, i) => (
                <View key={i} style={styles.subcategoryButtonContainer}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('categorySalons', { name: item })}
                    >
                        <Text style={styles.subcategoryButtonText}>{item}</Text>
                    </TouchableOpacity>
                </View>
            )) :
            null
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <View style={styles.logoImageContainer}></View>
                    <Icon
                        containerStyle={styles.searchIconContainer}
                        onPress={() => {
                            console.log('ok');
                        }}
                        name='search'
                        color='rgba(0,0,0, 0.6)'
                        size={25}
                    />
                </View>
                <ScrollView
                    style={{ width: '100%' }}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={styles.scrollContent}>
                        <View style={styles.swiperContainer}>
                            <Swiper
                                style={styles.swiper}
                                dot={<View style={styles.dot}></View>}
                                activeDot={<View style={styles.activeDot}></View>}
                                paginationStyle={{ bottom: 6 }}
                            >
                                <View style={styles.slideContainer}>
                                    <Image
                                        style={styles.image}
                                        source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                                    />
                                    <View style={styles.slideTextContainer}>
                                        <Text style={styles.slideTitle}>
                                            Лучший makeup{'\n'}для любого повода
                            </Text>
                                        <Text style={styles.slideDes}>
                                            от официального ужина{'\n'}до вечеринки
                            </Text>
                                    </View>
                                </View>
                                <View style={styles.slideContainer}>
                                    <Image
                                        style={styles.image}
                                        source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                                    />
                                    <View style={styles.slideTextContainer}>
                                        <Text style={styles.slideTitle}>
                                            Лучший makeup{'\n'}для любого повода
                            </Text>
                                        <Text style={styles.slideDes}>
                                            от официального ужина{'\n'}до вечеринки
                            </Text>
                                    </View>
                                </View>
                                <View style={styles.slideContainer}>
                                    <Image
                                        style={styles.image}
                                        source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                                    />
                                    <View style={styles.slideTextContainer}>
                                        <Text style={styles.slideTitle}>
                                            Лучший makeup{'\n'}для любого повода
                            </Text>
                                        <Text style={styles.slideDes}>
                                            от официального ужина{'\n'}до вечеринки
                            </Text>
                                    </View>
                                </View>
                                <View style={styles.slideContainer}>
                                    <Image
                                        style={styles.image}
                                        source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
                                    />
                                    <View style={styles.slideTextContainer}>
                                        <Text style={styles.slideTitle}>
                                            Лучший makeup{'\n'}для любого повода
                            </Text>
                                        <Text style={styles.slideDes}>
                                            от официального ужина{'\n'}до вечеринки
                            </Text>
                                    </View>
                                </View>
                            </Swiper>
                        </View>
                        <View style={styles.searchButonsContainer}>
                            {this._renderCategoryButtons()}
                        </View>
                        <View style={styles.subcategoryContainer}>
                            {this._renderSubcategorys()}
                        </View>
                        <View style={styles.salonsContainer}>
                            <View style={styles.textsRow}>
                                <Text style={styles.title}>
                                    Популярные салоны
                        </Text>
                                <TouchableOpacity
                                    style={styles.watchAllContainer}
                                >
                                    <Text style={styles.watchAll}>
                                        см.все
                            </Text>
                                </TouchableOpacity>

                            </View>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <View style={styles.horizontalScrollContent}>
                                    {this._renderPopularCard()}
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.choiceContainer}>
                            <View style={styles.textsRow}>
                                <Text style={styles.title}>
                                    Выбор редакции
                        </Text>
                                <TouchableOpacity
                                    style={styles.watchAllContainer}
                                >
                                    <Text style={styles.watchAll}>
                                        см.все
                            </Text>
                                </TouchableOpacity>

                            </View>
                            <ChoceCard item={{}} />
                        </View>
                        <View style={styles.newsContainer}>
                            <View style={styles.textsRow}>
                                <Text style={styles.title}>
                                    Новости и статьи
                        </Text>
                                <TouchableOpacity
                                    style={styles.watchAllContainer}
                                >
                                    <Text style={styles.watchAll}>
                                        см.все
                            </Text>
                                </TouchableOpacity>

                            </View>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <View style={styles.horizontalScrollContent}>
                                    {this._renderNewsCard()}
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.choiceContainer}>
                            <View style={styles.textsRow}>
                                <Text style={styles.title}>
                                    Выбор редакции
                        </Text>
                                <TouchableOpacity
                                    style={styles.watchAllContainer}
                                >
                                    <Text style={styles.watchAll}>
                                        см.все
                            </Text>
                                </TouchableOpacity>

                            </View>
                            {this._renderChoiceCard([1, 2, 3])}
                        </View>
                        <View style={styles.newsContainer}>
                            <View style={styles.textsRow}>
                                <Text style={styles.title}>
                                    Новое в фотоленте
                        </Text>
                                <TouchableOpacity
                                    style={styles.watchAllContainer}
                                >
                                    <Text style={styles.watchAll}>
                                        см.все
                            </Text>
                                </TouchableOpacity>

                            </View>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <View style={styles.horizontalScrollContent}>
                                    {this._renderNewPhotosCard()}
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export const Home = connect(
    state => ({ store: state })
)(HomeClass)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    scrollContent: {
        width: '100%',
        alignItems: 'center',
        marginBottom: 18,
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 90,
        width: '100%'
    },
    logoImageContainer: {
        backgroundColor: 'silver',
        height: '100%',
        width: 150
    },
    searchIconContainer: {
        marginRight: 14,
        marginTop: 20,
    },
    swiperContainer: {
        height: 170,
        width: '100%'
    },
    swiper: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    slideContainer: {
        flex: 1,
        marginHorizontal: '5%',
        backgroundColor: 'silver',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dot: {
        marginHorizontal: 3,
        height: 2,
        width: 15,
        backgroundColor: 'rgba(255,255,255,0.4)'
    },
    activeDot: {
        marginHorizontal: 3,
        height: 2,
        width: 15,
        backgroundColor: '#fff'
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 8,
    },
    slideTextContainer: {
        position: 'absolute',
        top: 32,
        left: 16
    },
    slideTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },
    slideDes: {
        marginTop: 6,
        color: '#fff',
        fontSize: 11
    },
    searchButonsContainer: {
        marginTop: 11,
        width: '90%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    btt: {
        flexDirection: 'row',
        marginVertical: 5,
        height: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText: {
        color: '#fff',
        marginHorizontal: 10,
        marginBottom: 3,
        fontSize: 13
    },
    salonsContainer: {
        //marginLeft: 16,
        width: '100%'
    },
    textsRow: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 64,
        alignItems: 'center',
    },
    title: {
        marginLeft: 16,
        marginTop: 10,
        fontSize: 19,
        fontWeight: 'bold'
    },
    watchAllContainer: {
        marginTop: 10,
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    watchAll: {
        color: '#379bff',
        fontSize: 15
    },
    popularCardContainer: {
        marginHorizontal: 6,
        width: 134,
        height: 180,
        backgroundColor: 'silver',
        borderRadius: 8
    },
    horizontalScrollContent: {
        flexDirection: 'row',
        marginHorizontal: 10,
    },
    popularCardInfoContainer: {
        position: 'absolute',
        left: 7,
        right: 5,
        bottom: 8
    },
    ratingRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    ratingText: {
        color: '#fff',
        fontSize: 13
    },
    popularCardInfoTitle: {
        fontSize: 16,
        color: '#fff',
        fontWeight: 'bold'
    },
    popularCardInfoDistance: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 12,

    },
    favoriteIconContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 5,
    },
    choiceContainer: {
        marginTop: 10,
        width: '100%',
        alignItems: 'center',
    },
    newsContainer: {
        //marginTop: 10,
        width: '100%'
    },
    newCardRatingRow: {
        marginTop: 4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    newsCardRatingText: {
        color: 'rgba(255,255,255,0.7)',
        fontSize: 13,
    },
    newsCardRatingTextCount: {
        marginHorizontal: 2,
        color: 'rgba(255,255,255,0.7)',
        fontSize: 13,
    },
    newsCardRatingIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    subcategoryContainer: {
        marginTop: 11,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '85%'
    },
    subcategoryButtonContainer: {
        height: 32,
        width: '50%',
        justifyContent: 'center',
    },
    subcategoryButtonText: {
        height: '100%',
        fontSize: 15,
        color: 'rgba(0,0,0,0.6)',
    }
});
