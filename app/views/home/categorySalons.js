import React from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView,
    Modal
} from 'react-native';
import {
    Icon,
    ButtonGroup
} from 'react-native-elements'
import { LinearGradient } from 'expo';
import ChoceCard from '../items/ChoiceCard'
import MapView, { Marker, ProviderPropType } from 'react-native-maps';

export class CategorySalons extends React.Component {
    map = null;

    constructor(props) {
        super(props)
        this.state = {
            selectedIndex: 0,
            markers: [
                {
                    latitude: 55.78904137,
                    longitude: 37.44907175,
                },
                {
                    latitude: 55.82222843,
                    longitude: 37.66055857,
                },
                {
                    latitude: 55.68234283,
                    longitude: 37.66742502,
                },
            ],
            searchSettingsModalVisible: false
        }
    }

    componentDidUpdate() {
        setTimeout(() => {
            if (this.map != null) {
                this.focusMap()
            }
        }, 1000)

    }

    focusMap() {
        this.map.fitToCoordinates(this.state.markers, {
            edgePadding: { top: 100, right: 100, bottom: 100, left: 100 },
            animated: true,
        });
    }

    selectIndex(index) {
        if (index == 0) {
            this.map = null
        }
        else {

        }
        this.setState({
            selectedIndex: index
        })
    }

    _renderMarkers() {
        return this.state.markers.map((coords, i) => (
            <Marker
                key={i}
                identifier={i.toString()}
                coordinate={coords}
            >
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.salonMarkerContainer}>
                        <View style={styles.salonMarkerDot}>
                        </View>
                    </View>
                    <Text style={styles.salonMarkerText}>Beauty Class</Text>
                </View>
            </Marker>
        ))
    }

    _renderCards() {
        let arr = [1, 2, 3, 4, 5]
        let times = ['9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '00:00',]
        return arr.map((item, i) => (
            <View key={i} style={styles.cardContainer}>
                <ChoceCard item={item} priceRating={true} />
                <View style={styles.timeContainer}>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    >
                        <View style={styles.timeScrollContent}>
                            {times.map((item, i) => (
                                <TouchableOpacity
                                    key={i}
                                    style={styles.timeItem}
                                >
                                    <Text style={styles.timeText}>
                                        {item}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </ScrollView>
                </View>
            </View>
        ))
    }

    _renderScroll(name) {
        this.map = null
        return (<View style={styles.contentContainer}>
            <View style={styles.scrollContent}>
                <ScrollView style={{ width: '100%' }}>
                    {this._renderCards()}
                </ScrollView>
            </View>
            <TouchableOpacity
                style={styles.reviewBttContainer}
                onPress={() => {
                    this.props.navigation.navigate('searchSettings', { name: name, tabBarVisible:true })
                }}
            >
                <LinearGradient
                    style={styles.reviewBtt}
                    start={[1, 1]}
                    //end={{ x: 0.5, y: 0.5 }}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                >
                    <Text style={styles.reviewBttText}>Уточнить параметры</Text>
                </LinearGradient>
            </TouchableOpacity>
        </View>)
    }

    _renderMap() {
        return (
            <View style={styles.contentContainer}>
                <MapView
                    ref={ref => this.map = ref}
                    style={{ flex: 1 }}
                    initialRegion={{
                        latitude: 55.755826,
                        longitude: 37.6172999,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                    {this._renderMarkers()}
                </MapView>
                <TouchableOpacity
                    style={styles.mapBtt1}>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.mapBtt2}>
                </TouchableOpacity>
            </View>
        )
    }

    _renderContent(name) {
        return this.state.selectedIndex == 0 ?
            this._renderScroll(name) :
            this._renderMap()
    }



    render() {
        const params = this.props.navigation.state.params
        let buttons = ['списком', 'на карте']


        return (<View style={styles.container}>
            <View style={styles.header}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <Icon
                        onPress={() => this.props.navigation.goBack()}
                        containerStyle={{
                            opacity: 0.7,
                            marginLeft: '2%'
                        }}
                        name='keyboard-arrow-left'
                        size={35}
                        color='silver'
                    />
                    <Text style={styles.headerTitle}>
                        Косметология
                        <Text style={styles.headerText}> {params.name.toLowerCase()}</Text>
                    </Text>
                </View>
            </View>
            <Text style={styles.contentTitle}>Лучшие салоны рядом с вами</Text>
            <View style={styles.optionsRow}>
                <ButtonGroup
                    onPress={(index) => this.selectIndex(index)}
                    selectedIndex={this.state.selectedIndex}
                    buttons={buttons}
                    containerStyle={styles.buttonGroupContainer}
                    textStyle={{
                        color: '#2391ff',
                        fontSize: 14
                    }}
                    underlayColor='#fff'
                    selectedButtonStyle={{ backgroundColor: '#2391ff' }}
                />
                <TouchableOpacity style={styles.optionsContainer}>
                    <Icon
                        name='tune'
                        color='#2391ff'
                        size={18}
                    />
                    <Text style={styles.optionsText}> ПАРАМЕТРЫ</Text>
                </TouchableOpacity>
            </View>
            {this._renderContent(params.name)}
        </View>
        )

    }
}

CategorySalons.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        marginTop: 26,
        marginBottom: 19,
        width: '100%',
        height: 50,
        justifyContent: 'center',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: -5,
    },
    headerText: {
        fontSize: 15,
        fontWeight: '100',
        color: 'rgba(0,0,0,0.6)',
    },
    contentTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: '5%',
    },
    reviewBttContainer: {
        height: 50,
        width: '100%',
    },

    reviewBtt: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    reviewBttText: {
        color: '#fff',
        fontSize: 18
    },
    optionsRow: {
        marginTop: 7,
        marginBottom: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    buttonGroupContainer: {
        marginLeft: '5%',
        height: 28,
        width: 144,
        borderRadius: 15,
        borderColor: '#2391ff',
        borderWidth: 1.3,
    },
    optionsContainer: {
        marginRight: '5%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    optionsText: {
        fontSize: 13,
        color: '#2391ff'
    },
    contentContainer: {
        flex: 1,
    },
    scrollContent: {
        flex: 1,
    },
    cardContainer: {
        marginBottom: 18,
        width: '100%',
        alignItems: 'center',
    },
    timeContainer: {
        width: '100%'
    },
    timeScrollContent: {
        marginHorizontal: 16,
        flexDirection: 'row',
    },
    timeItem: {
        marginVertical: 8,
        marginHorizontal: 4,
        justifyContent: 'center',
        alignItems: 'center',
        width: 64,
        height: 24,
        borderRadius: 12,
        borderWidth: 1.3,
        borderColor: '#2391ff',
    },
    timeText: {
        fontSize: 14,
        color: '#2391ff'
    },
    mapBtt1: {
        width: 42,
        height: 42,
        borderRadius: 21,
        backgroundColor: '#fff',
        position: 'absolute',
        left: 8,
        bottom: 25
    },
    mapBtt2: {
        width: 42,
        height: 42,
        borderRadius: 21,
        backgroundColor: '#fff',
        position: 'absolute',
        right: 8,
        bottom: 25
    },
    salonMarkerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#fff',
        backgroundColor: 'red'
    },
    salonMarkerDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#fff'
    },
    salonMarkerText: {
        fontSize: 12,
        fontWeight: 'bold'
    }
});
