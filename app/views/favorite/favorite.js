import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native';
import { Icon } from 'react-native-elements'
import { AirbnbRating } from 'react-native-ratings';
import ChoceCard from '../items/ChoiceCard'

export class FavoriteList extends Component {

  _renderChoiceCard(arr) {
    return arr.map((item, i) => (
      <ChoceCard favorite={true} key={i} item={item} />
    ))
  }

  _renderePersonItem() {
    return (
      <TouchableOpacity style={styles.personItemContainer}>
        <View style={styles.leftContainer}>
          <View style={styles.personPhotoContainer}>
            <Image
              style={styles.image}
              source={{ uri: 'http://eskipaper.com/images/beautiful-girls-wallpaper-2.jpg' }}
            />
            <Text style={styles.ratingTextInPhoto}>4,8</Text>
          </View>
          <View style={styles.popularCardInfoContainer}>
            <Text style={styles.popularCardInfoTitle}>
              Галина Селезнева
            </Text>
            <View style={styles.ratingRow}>
              <AirbnbRating
                showRating={false}
                count={5}
                defaultRating={5}
                size={10}
              />
              <Text style={styles.ratingText}>4,8/</Text>
              <Icon
                name='person'
                size={14}
                color='rgba(0,0,0,0.3)'
              />
              <Text style={styles.ratingText}>98</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => { }}
          style={{ padding: 8 }}
        >
          <Icon
            name='favorite'
            size={25}
            color='red'
          />
        </TouchableOpacity>
      </TouchableOpacity>
    )
  }

  _renderEmpty() {
    return (
      <View style={styles.emptyContainer}>
        <Text style={styles.emptyTitle}>
          Все лучшее - в одном месте
        </Text>
        <Text style={styles.emptyText}>
        Добавляйте салоны и мастеров
        </Text>
        <View style={styles.emptyTextRow}>
        <Text style={styles.emptyText}>
        в избранное нажатием на значок
        </Text>
        <Icon
        containerStyle={{marginLeft: 3}}
            name='favorite'
            size={20}
            color='red'
          />
        </View>
      </View>
    )
  }

  render() {
    return (

      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerTitle}>
            Любимые салоны и мастера
        </Text>
        </View>
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.scrollContent}>
            <ChoceCard favorite={true} item={{}} />
            {this._renderePersonItem()}
            {this._renderePersonItem()}
            {this._renderChoiceCard([1, 2])}
          </View>
          {this._renderEmpty()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  header: {
    marginTop: 26,
    marginBottom: 19,
    width: '100%',
    height: 50,
    justifyContent: 'center',
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    marginLeft: '5%',
  },
  scrollContent: {
    width: '100%',
    alignItems: 'center',
  },
  /////////////////////////////////
  personItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '90%',
    height: 45,
    marginBottom: 12,
  },
  personPhotoContainer: {
    marginLeft: 25,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  image: {
    flex: 1,
    width: '100%',
    borderRadius: 20,
  },
  ratingTextInPhoto: {
    position: 'absolute',
    bottom: 0,
    color: '#fff',
    fontSize: 13
  },
  popularCardInfoContainer: {
    marginLeft: 20,
  },
  ratingRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  ratingText: {
    color: 'rgba(0,0,0,0.7)',
    fontSize: 13
  },
  popularCardInfoTitle: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold'
  },
  favoriteIconContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 5,
  },
  leftContainer: {
    flexDirection: 'row',
  },
  emptyContainer: {
    alignSelf: 'center',
  },
  emptyTitle:{
    marginBottom: 16,
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold'
  },
  emptyText:{
    marginLeft: 14,
    color: 'black',
    fontSize: 14
  },
  emptyTextRow:{
    flexDirection: 'row',
    alignItems: 'center',
  }
});
