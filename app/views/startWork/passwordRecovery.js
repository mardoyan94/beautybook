import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements'

export class PasswordRecovery extends Component {
    constructor(props) {
        super(props)
        this.state = {
            number: ['•', '•', '•', '•'],
            number1:''
        }
    }

    _renderNumber() {
        let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        return arr.map((num, i) => <TouchableOpacity
            key={i}
            style={styles.numberContainer}
            onPress={() => {
                let numArr = this.state.number

                if (numArr.indexOf('•') >= 0) {
                    numArr[numArr.indexOf('•')] = num
                    this.setState({
                        number: numArr
                    })
                }
            }}
        >
            <Text style={styles.number}>{num}</Text>
        </TouchableOpacity>
        )
    }

    _renderCode() {
        return this.state.number.map((num) => {
            return num + ' '
        })
    }

    clear() {
        let arr = this.state.number
        for(let i=arr.length-1; i>= 0; i--){
            if(arr[i]!='•'){
                arr[i]='•'
                this.setState({
                    number: arr
                })
                break;
            }
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.contentContainer2}>
                    <View style={styles.iconContainer1}>
                        <Icon
                            onPress={() => this.props.navigation.goBack()}
                            name='keyboard-arrow-left'
                            color='silver'
                            size={40}
                        />
                    </View>
                    <Text style={styles.title}>Восстановление пароля</Text>
                    <Text style={styles.desc}>
                        Пожалуйста, введите код,{'\n'}
                        отправленный вам на телефон
                </Text>
                </View>
                <TouchableOpacity
                    onPress={() => { }}
                >
                    <Text style={styles.activText}>Отправить еще раз</Text>
                </TouchableOpacity>
                <View style={styles.codeContainer}>
                    <Icon
                        containerStyle={styles.iconContainer2}
                        onPress={() => this.clear()}
                        name='backspace'
                        color='rgba(0,0,0,0.5)'
                        size={19}
                    />
                    <Text style={styles.codeText}> {this._renderCode()}</Text>
                    <View style={styles.keyboardContainer}>
                        {this._renderNumber()}
                        <View style={styles.numberContainer}></View>
                        <TouchableOpacity
                            style={styles.numberContainer}
                            onPress={() => {
                                let numArr = this.state.number

                                if (numArr.indexOf('•') >= 0) {

                                    numArr[numArr.indexOf('•')] = 0
                                    this.setState({
                                        number: numArr
                                    })
                                }
                            }}
                        >
                            <Text style={styles.number}>0</Text>
                        </TouchableOpacity>
                        <View
                            style={styles.numberContainer}

                        >
                            <Icon
                                onPress={() => this.props.navigation.navigate('newPassword')}
                                name='done'
                                size={30}
                                color='#566efe'
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    contentContainer2: {
        marginTop: 40,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    desc: {
        marginTop: 15,
        fontSize: 15,
        textAlign: 'center'
    },
    activText: {
        color: '#379bff',
        //borderBottomWidth: 2,
        //borderBottomColor: '#379bff',
        fontSize: 14,
        fontWeight: 'bold'
    },
    keyboardContainer: {
        flexDirection: 'row',
        width: '83%',
        height: '80%',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
    },
    numberContainer: {
        height: '25%',
        width: '33%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    number: {
        fontSize: 30,
        color: 'rgba(0,0,0,0.5)'
    },
    codeText: {
        marginTop: '4%',
        fontSize: 30
    },
    codeContainer: {
        marginBottom: 30,
        height: '54%',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    iconContainer1: {
        position: 'absolute',
        left: 16,
        top: -5
    },
    iconContainer2: {
        height: 30,
        width: 40,
        position: 'absolute',
        top: '7%',
        right: '10%'
    }
});
