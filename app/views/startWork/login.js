import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Keyboard,
    Modal,
    ScrollView,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native';
import { LinearGradient } from 'expo';
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';

class LoginClass extends Component {
    constructor(props) {
        super(props)
        this.state = {
            password: '',
            passwordVal: false,
            keyboardHeight: 0,
          keyboardOpen: false,
          modalVisible: false,
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          this.keyboardDidShowListener,
        );
        this.keyboardHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this.keyboardHideListener,
          );
      }
      keyboardDidShowListener = e => {
        this.setState({
          keyboardOpen: true,
          keyboardHeight: e.endCoordinates.height,
        });
      };

      keyboardHideListener = () => {
        this.setState({
            keyboardHeight: 0,
          keyboardOpen: false,
        });
      };

      async setTokens(){
          try{
            await AsyncStorage.setItem('token', 'token')
          }
          catch(error){

          }
         
      }

      login(){
        Keyboard.dismiss()
        this.setState({
            modalVisible: true
        })
        setTimeout(() => {
            this.setTokens().then(()=>{
                this.setState({
                    modalVisible: false
                })
                this.props.dispatch({type:'SET_CONFIG', logOut: false})
            }).catch((error)=>{
                console.log(error);
                
            })
            
            console.log('ok');
            
        }, 2000)
      }

      _renderModal() {
        return <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                alert('Modal has been closed.');
            }}>
            <View style={styles.modalContainer}>
                <View style={styles.modalAlertContainer}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.alertTitle}>Успешно</Text>
                    </View>
                    <Text style={styles.alertDesc}>
                        Добро пожаловать{'\n'}
                        в Beauty Book!
                </Text>
                </View>
            </View>
        </Modal>
    }

    passwordValidation(text) {
        var val = /^.{6,}$/
        if (val.test(text)) {
            this.setState({
                password: text,
                passwordVal: true
            })
        }
        else {
            this.setState({
                passwordVal: false
            })
        }
    }

    render() {
        return (
            <ScrollView
            keyboardShouldPersistTaps='handled'
            style={{ marginBottom: this.state.keyboardHeight, backgroundColor: '#fff' }} 
            >
            <View style={styles.container}>
                <View style={styles.contentContainer2}>
                    <View style={styles.iconContainer1}>
                        <Icon
                            onPress={() => {
                                Keyboard.dismiss()
                                this.props.navigation.goBack()
                            }}
                            name='keyboard-arrow-left'
                            color='silver'
                            size={40}
                        />
                    </View>
                    <Text style={styles.title}>Вход в аккаунт</Text>
                </View>
                <Text style={styles.inputTitle1}>введите ваш пароль</Text>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        underlineColorAndroid='transparent'
                        onChangeText={(text) => this.passwordValidation(text)}
                    />
                </View>
                <View style={styles.activTextContainer}>
                <TouchableOpacity
                    style={{alignSelf: 'flex-start'}}
                    onPress={()=>this.props.navigation.navigate('passwordRecovery')}
                    >
                    <Text style={styles.activText}>Забыли пароль?</Text>
                    <View style={styles.activTextLine}></View>
                    </TouchableOpacity>
                    </View>
                <TouchableOpacity
                keyboardShouldPersistTaps={true}
                    onPress={() => this.login()}
                    disabled={!this.state.passwordVal}
                     style={[styles.bttContainer, !this.state.passwordVal ? { opacity: 0.5 } : null ]}
                >
                    <LinearGradient
                        style={styles.btt}
                        start={[1, 1]}
                        //end={{ x: 0.5, y: 0.5 }}
                        locations={[0.3, 0.7]}
                        colors={['#7c52fd', '#1998ff']}
                    >
                        <Text style={styles.bttText}>Далее</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
            {this._renderModal()}
            </ScrollView>

        );
    }
}

export const Login = connect()(LoginClass)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        //justifyContent: 'space-between',
    },
    imageContainer: {
        marginTop: '16%',
        marginBottom: '14%',
        width: '100%',
        height: 138,
        backgroundColor: 'silver'
    },
    contentContainer3: {
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '4%',
    },
    inputTitle1: {
        marginTop: '17%',
        fontSize: 14,
        color: 'rgba(0,0,0,0.4)'
    },
    inputTitle2: {
        marginTop: '5%',
        fontSize: 14,
        color: 'rgba(0,0,0,0.4)'
    },
    bttContainer: {
        marginTop: 24,
        marginBottom: 24,
        height: 50,
        width: '75%',
    },

    btt: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText: {
        color: '#fff',
        fontSize: 18
    },

    inputContainer: {
        width: '80%',
        borderBottomColor: 'rgba(0,0,0,0.08)',
        borderBottomWidth: 2,
    },
    input: {
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
    },
    iconContainer1: {
        position: 'absolute',
        left: 16,
        top: -5
    },
    contentContainer2: {
        marginTop: 75,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    modalContainer:{
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalAlertContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '78%',
        height: 146,
        backgroundColor: '#fff',
        borderRadius: 10,
    },
    alertTitle:{
        fontSize: 19,
        fontWeight: 'bold',
    },
    alertDesc: {
        marginTop: 18,
        fontSize: 16,
        textAlign: 'center'
    },
    activTextContainer:{
        marginTop: 30,
        marginBottom: 5,
        width: '80%'
    },
    activText:{
        color: '#379bff',
        fontSize: 14,
        alignSelf: 'flex-start'
    },
    activTextLine:{
        marginTop: -3,
        borderBottomWidth: 1,
        borderBottomColor: '#379bff',
    }
});
