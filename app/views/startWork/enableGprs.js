import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';

export class EnableGprs extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                
                <View style={styles.contentContainer2}>
                <View style={styles.contentContainer1}></View>
                    <Text style={styles.title}>Включите геолокацию</Text>
                    <Text style={styles.desc}>
                        Это позволит вам получать подборки{'\n'}
                        организации, основанные на вашем {'\n'}
                        месторасположении, а также истать{'\n'}
                        ближайшие вам салоны на карте.
                </Text>
                </View>
                <View style={styles.contentContainer3}>
                    <TouchableOpacity
                    onPress={()=>this.props.navigation.navigate('enterNumber')}
                    style={styles.bttContainer}
                    >
                    <LinearGradient
                    style={styles.btt}
                    start={[1, 1]}
                    //end={{ x: 0.5, y: 0.5 }}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                     >
                        <Text style={styles.bttText}>Включить</Text>
                    </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                    onPress={()=>this.props.navigation.navigate('enterNumber')}
                    style={styles.bttContainerReject}
                    >
                    <Text style={styles.bttTextReject}>Не сейчас</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    contentContainer1: {
        backgroundColor: 'silver',
        height: 200,
        width: '100%'
    },
    contentContainer2: {
        marginTop: '20%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentContainer3:{
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '4%',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    desc: {
        marginTop: 15,
        fontSize: 15,
        textAlign: 'center'
    },
    bttContainer:{
        height: 50,
        width: '100%',
    },
    bttContainerReject:{
        marginTop: 16,
        height: 50,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btt:{
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText:{
        color: '#fff',
        fontSize: 18
    },
    bttTextReject:{
        color: 'rgba(0,0,0,0.5)',
        fontSize: 18,
    },
});
