import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Keyboard,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import { LinearGradient } from 'expo';
import { Icon } from 'react-native-elements'

export class Password extends Component {
    constructor(props) {
        super(props)
        this.state = {
            password1: '',
            password2: '',
            password1Val: false,
            password2Val: false,
            modalVisible: false,
            keyboardHeight: 0,
            keyboardOpen: false,
            inputError: false
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this.keyboardDidShowListener,
        );
        this.keyboardHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this.keyboardHideListener,
        );
    }
    keyboardDidShowListener = e => {
        this.setState({
            keyboardOpen: true,
            keyboardHeight: e.endCoordinates.height,
        });
    };

    keyboardHideListener = () => {
        this.setState({
            keyboardHeight: 0,
            keyboardOpen: false,
        });
    };

    password1Validation(text) {
        if(this.state.inputError){
            this.setState({
                inputError: false
            })
        }
        var val = /^.{6,}$/
        if (val.test(text)) {
            this.setState({
                password1: text,
                password1Val: true
            })
        }
        else {
            this.setState({
                password1Val: false
            })
        }
    }

    password2Validation(text) {
        if(this.state.inputError){
            this.setState({
                inputError: false
            })
        }
        var val = /^.{6,}$/
        if (val.test(text)) {
            this.setState({
                password2: text,
                password2Val: true
            })
        }
        else {
            this.setState({
                password2Val: false
            })
        }
    }

    allValid() {
        if (this.state.password1Val, this.state.password2Val) {
            return true;
        }
        else {
            return false;
        }
    }

    login(){
        Keyboard.dismiss()
        if(this.state.password1==this.state.password2){
            this.setState({
                modalVisible: true
            })
            setTimeout(() => {
                this.setState({
                    modalVisible: false
                })
            }, 2000)
        }
        else{
            this.setState({
                inputError: true
            })
        }
       
    }

    _renderModal() {
        return <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                alert('Modal has been closed.');
            }}>
            <View style={styles.modalContainer}>
                <View style={styles.modalAlertContainer}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.alertTitle}>Успешно</Text>
                    </View>
                    <Text style={styles.alertDesc}>
                        Добро пожаловать{'\n'}
                        в Beauty Book!
                </Text>
                </View>
            </View>
        </Modal>
    }

    render() {
        return (
            <ScrollView
                keyboardShouldPersistTaps='handled'
                style={{ marginBottom: this.state.keyboardHeight, backgroundColor: '#fff' }}
            >
                <View style={styles.container}>
                    <View style={styles.contentContainer2}>
                        <View style={styles.iconContainer1}>
                            <Icon
                                onPress={() => {
                                    Keyboard.dismiss()
                                    this.props.navigation.goBack()
                                }}
                                name='keyboard-arrow-left'
                                color='silver'
                                size={40}
                            />
                        </View>
                        <Text style={styles.title}>Регистрация</Text>
                    </View>
                    <Text style={styles.inputTitle1}>придумайте пароль</Text>
                    <View style={styles.inputContainer1}>
                        <TextInput
                            style={styles.input}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.password1Validation(text)}
                        />
                    </View>
                    <Text style={styles.inputTitle2}>повторите пароль</Text>
                    <View style={[styles.inputContainer2, this.state.inputError ? { borderBottomColor: 'red' } : null]}>
                        <TextInput
                            style={styles.input}
                            underlineColorAndroid='transparent'
                            onChangeText={(text) => this.password2Validation(text)}
                        />
                    </View>
                    <TouchableOpacity
                        keyboardShouldPersistTaps={true}
                        onPress={() => this.login()}
                        disabled={!this.allValid()}
                        style={[styles.bttContainer, !this.allValid() ? { opacity: 0.5 } : null]}
                    >
                        <LinearGradient
                            style={styles.btt}
                            start={[1, 1]}
                            //end={{ x: 0.5, y: 0.5 }}
                            locations={[0.3, 0.7]}
                            colors={['#7c52fd', '#1998ff']}
                        >
                            <Text style={styles.bttText}>Далее</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                {this._renderModal()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        //justifyContent: 'space-between',
    },
    imageContainer: {
        marginTop: '16%',
        marginBottom: '14%',
        width: '100%',
        height: 138,
        backgroundColor: 'silver'
    },
    contentContainer3: {
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '4%',
    },
    inputTitle1: {
        marginTop: '17%',
        fontSize: 14,
        color: 'rgba(0,0,0,0.4)'
    },
    inputTitle2: {
        marginTop: '5%',
        fontSize: 14,
        color: 'rgba(0,0,0,0.4)'
    },
    bttContainer: {
        marginTop: 24,
        marginBottom: 24,
        height: 50,
        width: '75%',
    },

    btt: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText: {
        color: '#fff',
        fontSize: 18
    },

    inputContainer1: {
        width: '80%',
        borderBottomColor: 'rgba(0,0,0,0.08)',
        borderBottomWidth: 2,
    },
    inputContainer2: {
        width: '80%',
        borderBottomColor: 'rgba(0,0,0,0.08)',
        borderBottomWidth: 2,
    },
    input: {
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
    },
    iconContainer1: {
        position: 'absolute',
        left: 16,
        top: -5
    },
    contentContainer2: {
        marginTop: 75,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalAlertContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '78%',
        height: 146,
        backgroundColor: '#fff',
        borderRadius: 10,
    },
    alertTitle: {
        fontSize: 19,
        fontWeight: 'bold',
    },
    alertDesc: {
        marginTop: 18,
        fontSize: 16,
        textAlign: 'center'
    },
});
