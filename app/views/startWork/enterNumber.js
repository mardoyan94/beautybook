import React, { Component } from 'react';
import { 
    StyleSheet,
     Text,
      View,
       TouchableOpacity,
        TextInput,
        Keyboard,
        ScrollView
    } from 'react-native';
import { LinearGradient } from 'expo';
import CountryPicker from 'react-native-country-picker-modal'

export class EnterNumber extends Component {
    constructor(props) {
        super(props)
        this.state={
            number: '+7(999) 809-668-69',
            numberVal: false,
            keyboardHeight: 0,
          keyboardOpen: false,
          cca2: 'RU',
          callingCode: '7'
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          this.keyboardDidShowListener,
        );
        this.keyboardHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this.keyboardHideListener,
          );
      }
      keyboardDidShowListener = e => {
        this.setState({
          keyboardOpen: true,
          keyboardHeight: e.endCoordinates.height,
        });
      };

      keyboardHideListener = () => {
        this.setState({
            keyboardHeight: 0,
          keyboardOpen: false,
        });
      };

      numberValidation(text) {
        var val = /^.{6,}$/
        if (val.test(text)) {
            this.setState({
                number: text,
                numberVal: true
            })
        }
        else {
            this.setState({
                numberVal: false
            })
        }
    }

    render() {
        return (
            <ScrollView
            keyboardShouldPersistTaps='handled'
            style={{ marginBottom: this.state.keyboardHeight, backgroundColor: '#fff' }} 
            >
            <View style={styles.container}>
                <View style={styles.imageContainer}></View>
                <Text style={styles.title}>Номер телефона</Text>
                <View style={styles.inputContainer}>
                <CountryPicker
          //countryList={NORTH_AMERICA}
          onChange={value => {
            this.setState({ cca2: value.cca2, callingCode: value.callingCode })
          }}
          cca2={this.state.cca2}
          translation="eng"
        />
        <Text style={styles.countryCode}>+({this.state.callingCode})</Text>
                <TextInput
                keyboardType='numeric'
                style={styles.input}
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.numberValidation(text)}
                />
                </View>
                    <TouchableOpacity
                    disabled={!this.state.numberVal}
                    onPress={()=>this.props.navigation.navigate('login')}
                    style={[styles.bttContainer, !this.state.numberVal ? { opacity: 0.5 } : null ]}
                    >
                    <LinearGradient
                    style={styles.btt}
                    start={[1, 1]}
                    //end={{ x: 0.5, y: 0.5 }}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                     >
                        <Text style={styles.bttText}>Далее</Text>
                    </LinearGradient>
                    </TouchableOpacity>
                </View>
                </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        //justifyContent: 'space-between',
    },
    imageContainer:{
        marginTop: '16%',
        marginBottom: '14%',
        width: '100%',
        height: 138,
        backgroundColor: 'silver'
    },
    contentContainer3:{
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '4%',
    },
    title: {
        fontSize: 14,
        color: 'rgba(0,0,0,0.4)'
    },

    bttContainer:{
        marginTop: 24,
        marginBottom: 24,
        height: 50,
        width: '75%',
    },

    btt:{
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText:{
        color: '#fff',
        fontSize: 18
    },

    inputContainer:{
        alignItems: 'center',
        flexDirection: 'row',
        width: '80%',
        borderBottomColor: 'rgba(0,0,0,0.08)',
        borderBottomWidth: 2,
    },
    input:{
        flex: 1,
        fontSize: 15,
        //textAlign: 'center',
        color: 'black',
    },
    countryCode:{
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
    }
});
