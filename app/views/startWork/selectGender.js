import React, { Component } from 'react';
import {
     StyleSheet,
     Text,
      View,
      TouchableOpacity, 
    } from 'react-native';
import { LinearGradient } from 'expo';

export class SelectGender extends Component {
    constructor(props) {
        super(props)
        this.state={
            male: false,
            female: true
        }
    }   

    _renderCheckboxFemale(){
        return this.state.female ?
        <TouchableOpacity
                style={styles.genderCheckboxActiv}
                >
                <LinearGradient
                    style={styles.genderCheckboxGradientContainer}
                    start={[1, 1]}
                    //end={{ x: 0.5, y: 0.5 }}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                     >
                        <Text style={styles.genderCheckBoxTextActiv}>Женщина</Text>
                        <View style={styles.genderCheckBoxCircleActiv}>
                        <View style={styles.genderCheckboxDot}></View>
                        </View>
                    </LinearGradient>
                </TouchableOpacity> :
        <TouchableOpacity
        onPress={()=>{
            this.setState({
                female: true,
                male: false
            })
        }}
                style={styles.genderCheckbox}
                >
                <Text style={styles.genderCheckBoxText}>Женщина</Text>
                <View style={styles.genderCheckBoxCircle}></View>
                </TouchableOpacity>
                
    }

    _renderCheckboxMale(){
        return this.state.male ?
        <TouchableOpacity
                style={[styles.genderCheckboxActiv, { marginTop: 16 }]}
                >
                <LinearGradient
                    style={styles.genderCheckboxGradientContainer}
                    start={[1, 1]}
                    //end={{ x: 0.5, y: 0.5 }}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                     >
                        <Text style={styles.genderCheckBoxTextActiv}>Мужчина</Text>
                        <View style={styles.genderCheckBoxCircleActiv}>
                        <View style={styles.genderCheckboxDot}></View>
                        </View>
                    </LinearGradient>
                </TouchableOpacity> :
        <TouchableOpacity
        onPress={()=>{
            this.setState({
                male: true,
                female: false
            })
        }}
                style={[styles.genderCheckbox, { marginTop: 16 }]}
                >
                <Text style={styles.genderCheckBoxText}>Мужчина</Text>
                <View style={styles.genderCheckBoxCircle}></View>
                </TouchableOpacity>
                
    }

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.contentContainer1}>
                <Text style={styles.title}>Укажите ваш пол</Text>
                <Text style={styles.desc}>Мы используем данную информацию{'\n'}
                    для улучшения индивидуальной{'\n'}
                    подборки салонов и статей.</Text>
                </View>
                <View style={styles.contentContainer2}>
                {this._renderCheckboxFemale()}
                {this._renderCheckboxMale()}
                </View>
                <View style={styles.contentContainer3}>
                    <TouchableOpacity
                    onPress={()=>{}}
                    >
                    <Text style={styles.activText}>вход для мастеров</Text>
                    <View style={styles.activTextLine}></View>
                    </TouchableOpacity>
                    <TouchableOpacity
                    onPress={()=>this.props.navigation.navigate('gprs')}
                    style={styles.bttContainer}
                    >
                    <LinearGradient
                    style={styles.btt}
                    start={[1, 1]}
                    //end={{ x: 0.5, y: 0.5 }}
                    locations={[0.3, 0.7]}
                    colors={['#7c52fd', '#1998ff']}
                     >
                        <Text style={styles.bttText}>Далее</Text>
                    </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    contentContainer1:{
        marginTop: '25%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentContainer2:{
        alignItems: 'center',
        justifyContent: 'center',
        width: '47%'
    },
    contentContainer3:{
        width: '75%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
    },
    desc:{
        marginTop: 15,
        fontSize: 15,
        textAlign: 'center'
    },
    activText:{
        color: '#379bff',
        borderBottomWidth: 2,
        borderBottomColor: '#379bff',
        fontSize: 14,
    },
    activTextLine:{
        marginTop: -5,
        borderBottomWidth: 1,
        borderBottomColor: '#379bff',
    },
    bttContainer:{
        marginTop: 32,
        height: 50,
        width: '100%',
    },
    btt:{
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText:{
        color: '#fff',
        fontSize: 18
    },
    genderCheckbox:{
        width: '100%',
        height: 40,
        borderRadius: 20,
        borderColor: '#3398ff',
        borderWidth: 1.5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    genderCheckBoxText:{
        marginLeft: 14,
        fontSize: 19,
        color: '#3398ff'
    },
    genderCheckBoxCircle:{
        marginRight: 10,
        height: 20,
        width: 20,
        borderColor: '#3398ff',
        borderWidth: 2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    genderCheckboxActiv:{
        width: '100%',
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    genderCheckboxGradientContainer:{
        width: '100%',
        height: '100%',
        borderRadius: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    genderCheckBoxTextActiv:{
        marginLeft: 14,
        fontSize: 19,
        color: '#fff'
    },
    genderCheckBoxCircleActiv:{
        marginRight: 12,
        height: 20,
        width: 20,
        borderColor: '#fff',
        borderWidth: 2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    genderCheckboxDot:{
        width:8,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#fff'
    }

});
