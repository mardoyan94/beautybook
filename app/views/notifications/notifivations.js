import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native';
import { Icon } from 'react-native-elements'
import { LinearGradient } from 'expo';

export class NotificationsList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            discounts: false,
            record: false,
            update: false
        }
    }

    _renderEmpty() {
        return (
            <View style={styles.emptyContainer}>
                <Text style={styles.emptyTitle}>
                    Все лучшее - в одном месте
        </Text>
                <Text style={styles.emptyText}>
                    Добавляйте салоны и мастеров
        </Text>
                <View style={styles.emptyTextRow}>
                    <Text style={styles.emptyText}>
                        в избранное нажатием на значок
        </Text>
                    <Icon
                        containerStyle={{ marginLeft: 3 }}
                        name='favorite'
                        size={20}
                        color='red'
                    />
                </View>
            </View>
        )
    }

    _renderRecordNotificationItem() {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (this.state.record) {
                        this.setState({
                            record: false
                        })
                    }
                    else {
                        this.setState({
                            record: true
                        })
                    }
                }}
                style={styles.recordNotificationItemContainer}
            >
                <View style={styles.notificationIconContainer}></View>
                <View style={styles.notificationTextsContainer}>
                    <View style={styles.notificationTitleRow}>
                        <Text style={styles.notificationTitle}>
                        Подтверждение{'\n'}записи
                        </Text>
                        <Text style={styles.notificationTimeText}>
                        11:45
                        </Text>

                    </View>
                    <Text
                        style={styles.notificationText1}
                        numberOfLines={this.state.record ? 100 : 2}
                    >
                        Вы успешо записались на стрижку в салоне TopGun в
                        11:00 5.08.17 Вы успешо записались на стрижку
                    </Text>
                    {this.state.record ?
                        <View>
                            <Text style={styles.notificationText2}>Акция продлится до 31.10.17</Text>
                            <TouchableOpacity
                                onPress={() => { }}
                                style={styles.bttContainer}
                            >
                                <LinearGradient
                                    style={styles.btt}
                                    start={[1, 1]}
                                    //end={{ x: 0.5, y: 0.5 }}
                                    locations={[0.3, 0.7]}
                                    colors={['#7c52fd', '#1998ff']}
                                >
                                    <Text style={styles.bttText}>Подтвердить</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View> :
                        null
                    }
                </View>
            </TouchableOpacity>
        )
    }

    _renderDiscountsNotificationItem() {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (this.state.discounts) {
                        this.setState({
                            discounts: false
                        })
                    }
                    else {
                        this.setState({
                            discounts: true
                        })
                    }
                }}
                style={styles.recordNotificationItemContainer}
            >
                <View style={styles.notificationIconContainer}></View>
                <View style={styles.notificationTextsContainer}>
                    <View style={styles.notificationTitleRow}>
                        <Text style={styles.notificationTitle}>
                            Скидки в BoyCut!
                        </Text>
                        <Text style={styles.notificationTimeText}>
                            01.10.17
                        </Text>

                    </View>
                    <Text
                        style={styles.notificationText1}
                        numberOfLines={this.state.discounts ? 100 : 2}
                    >
                        Успей подстричься с 15% скидкой, предъявив мастеру промокод НЕБРИТЯБРЬ.
                    </Text>
                    {this.state.discounts ?
                        <View>
                            <Text style={styles.notificationText2}>Акция продлится до 31.10.17</Text>
                            <TouchableOpacity
                                onPress={() => { }}
                                style={styles.bttContainer}
                            >
                                <LinearGradient
                                    style={styles.btt}
                                    start={[1, 1]}
                                    //end={{ x: 0.5, y: 0.5 }}
                                    locations={[0.3, 0.7]}
                                    colors={['#7c52fd', '#1998ff']}
                                >
                                    <Text style={styles.bttText}>Перейти в салон</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View> :
                        null
                    }
                </View>
            </TouchableOpacity>
        )
    }

    _renderUpdateNotificationItem() {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (this.state.update) {
                        this.setState({
                            update: false
                        })
                    }
                    else {
                        this.setState({
                            update: true
                        })
                    }
                }}
                style={styles.recordNotificationItemContainer}
            >
                <View style={styles.notificationIconContainer}></View>
                <View style={styles.notificationTextsContainer}>
                    <View style={styles.notificationTitleRow}>
                        <Text style={styles.notificationTitle}>
                        Обновление приложения
                        </Text>
                        <Text style={styles.notificationTimeText}>
                            01.10.17
                        </Text>

                    </View>
                    <Text
                        style={styles.notificationText1}
                        numberOfLines={this.state.update ? 100 : 2}
                    >
                        Сегодня мы обновили ваше приложение сделав его ещо лучше и удобнее!
                    </Text>
                    {this.state.update ?
                        <View>
                            <Text style={styles.notificationText2}>Акция продлится до 31.10.17</Text>
                            <TouchableOpacity
                                onPress={() => { }}
                                style={styles.bttContainer}
                            >
                                <LinearGradient
                                    style={styles.btt}
                                    start={[1, 1]}
                                    //end={{ x: 0.5, y: 0.5 }}
                                    locations={[0.3, 0.7]}
                                    colors={['#7c52fd', '#1998ff']}
                                >
                                    <Text style={styles.bttText}>Спасибо</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View> :
                        null
                    }
                </View>
            </TouchableOpacity>
        )
    }


    render() {
        return (

            <View style={styles.container}>

                <View style={styles.header}>
                    <Text style={styles.headerTitle}>
                    Уведомления
        </Text>
                </View>
                <ScrollView style={{ width: '100%' }}>
                    <View style={styles.scrollContent}>
                        {this._renderRecordNotificationItem()}
                        {this._renderUpdateNotificationItem()}
                        {this._renderDiscountsNotificationItem()}
                    </View>
                    {/* {this._renderEmpty()} */}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    header: {
        marginTop: 26,
        marginBottom: 19,
        width: '100%',
        height: 50,
        justifyContent: 'center',
    },
    headerTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginLeft: '5%',
    },
    scrollContent: {
        width: '100%',
        alignItems: 'center',
    },

    emptyContainer: {
        alignSelf: 'center',
    },
    emptyTitle: {
        marginBottom: 16,
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold'
    },
    emptyText: {
        marginLeft: 14,
        color: 'black',
        fontSize: 14
    },
    emptyTextRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    recordNotificationItemContainer: {
        width: '100%',
        marginBottom: 50,
        flexDirection: 'row',
    },
    notificationTitleRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
    },
    notificationTitle: {
        color: 'black',
        fontSize: 17,
        fontWeight: 'bold',
        flex: 1,
        flexWrap: 'wrap'
    },
    notificationIconContainer: {
        width: 70,
        backgroundColor: 'silver'
    },
    notificationTimeText: {
        marginTop: 8,
        color: 'rgba(0,0,0,0.7)',
        fontSize: 12,
        marginHorizontal: 16,
    },
    notificationTextsContainer: {
        flex: 1
    },
    notificationText1: {
        marginTop: 3,
        marginRight: 25,
    },
    notificationText2: {
        marginTop: 16,
        marginRight: 25,
    },
    bttContainer: {
        marginTop: 30,
        marginLeft: 5,
        height: 50,
        width: '75%',
    },

    btt: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bttText: {
        color: '#fff',
        fontSize: 18
    },
});
